<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
class CheckoutAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $new_address_rules = [
            'name' => 'required',
            'detail' => 'required',
            'province_id' => 'required|exists:provinces,id',
            'regency_id' => 'required|exists:regencies,id',
            'phone' => 'required|digits_between:7,15'
        ];
        if (Auth::check()) {
            // mengambil data id address berdasarkan user
            $address_limit = implode(',', Auth::user()->addresses->lists('id')->all()) . ',new-address';
            // jika tidak ada yg cocok maka new
            $rules = ['address_id' => 'required|in:' . $address_limit];
            // mencocokan id address dengan yang ada
            if ($this->get('address_id') == 'new-address') {
                return $rules += $new_address_rules;
            }
            return $rules;
        }
        return $new_address_rules;
    }
}
