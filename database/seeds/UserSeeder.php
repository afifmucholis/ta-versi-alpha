<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample admin
        App\User::create([
        	'name' => 'admin',
        	'email' => 'admin@gmail.com',
        	'password' => bcrypt('admin'),
        	'role' => 'admin'
        	]);
        // sample customer
        App\User::create([
        	'name' => 'customer',
        	'email' => 'customer@gmail.com',
        	'password' => bcrypt('customer'),
        	'role' => 'customer'
        	]);
    }
}
